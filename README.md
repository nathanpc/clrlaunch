# clrLaunch

The fullscreen application launcher for Linux.

Keep in mind that I suck at C/C++. My background is JavaScript, so this is something almost completely new for me. Feel free to correct me and please submit Pull Requests with corrections and better ways to do things.

# Goal

The main goal with this application is to create a awesome full screen, keyboard controlled, launcher for your programs. Why? Because it will be a lot better when you're using your Linux box connected to a TV. Here's the mockup for the first version:

![Mockup](http://i.imgur.com/qBSvp.png)

# History

A brief history of the things that are getting added to the project before the first "stable" version.

 - Just displays a gray square at the time
 - Added most of the code I was using to learn OpenGL