// collision.cpp
// Detects Collisions

#include <iostream>
#include <cmath>

#include "../helpers/coords.h"

bool rectCircleCollision(coords circle, coords rect, int radius, int rect_width, int rect_height) {
    coords circleDistance;
    float cornerDistance_sq;
    int width = rect_width * 2;
    int height = rect_height * 2;
    
    circleDistance.x = abs(circle.x - rect.x);
    circleDistance.y = abs(circle.y - rect.y);
    
    if (circleDistance.x > (width / 2 + radius)) { return false; }
    if (circleDistance.y > (height / 2 + radius)) { return false; }
    
    if (circleDistance.x <= (width / 2)) { return true; } 
    if (circleDistance.y <= (height / 2)) { return true; }
    
    cornerDistance_sq = pow((circleDistance.x - width / 2), 2) + pow((circleDistance.y - height / 2), 2);
    
    return (cornerDistance_sq <= (radius ^ 2));
}