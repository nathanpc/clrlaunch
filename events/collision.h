#ifndef COLLISION_H
#define COLLISION_H

#include "../helpers/coords.h"

bool rectCircleCollision(coords circle, coords rect, int radius, int rect_width, int rect_height);

#endif