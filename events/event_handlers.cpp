// event_handlers.cpp
// Handle events

#include <iostream>

// Include the OpenGL header files.
#ifdef __APPLE__
#include <OpenGL/OpenGL.h>
#include <GLUT/GLUT.h>
#else
#include <GL/glut.h>
#endif

#include "../scene/scene.h"

void handleMouse(int button, int state, int x, int y) {
    if (state == GLUT_DOWN) {
        if (button == GLUT_LEFT_BUTTON) {
            //coords pos = { x, y };
        }
    }
}

/** Handles key presses.
 
 @param key Key pressed
 @param x Mouse x position
 @param y Mouse y position
 */
void handleKeypress(unsigned char key, int x, int y) {
    switch (key) {
        case 27:  // Escape key
            exit(0);
    }
}

// Handles the screen resize.
void handleResize(int w, int h) {
    // Tell OpenGL how to convert from coordinates to pixel values.
	glViewport(0, 0, w, h);
    
    glMatrixMode(GL_PROJECTION);  // Switch to setting the camera perspective.
    
    // Set the camera perspective.
	glLoadIdentity();  // Reset the camera.
    gluPerspective(45.0, (double)w / (double)h, 1.0, 200.0);
}