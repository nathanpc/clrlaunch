#ifndef EVENT_HANDLERS_H
#define EVENT_HANDLERS_H

void handleMouse(int button, int state, int x, int y);
void handleKeypress(unsigned char key, int x, int y);
void handleResize(int w, int h);

#endif