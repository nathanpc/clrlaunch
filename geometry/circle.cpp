// circle.cpp
// Circle stuff

#include <cmath>

// Include the OpenGL header files.
#ifdef __APPLE__
#include <OpenGL/OpenGL.h>
#include <GLUT/GLUT.h>
#else
#include <GL/glut.h>
#endif

#include "../helpers/coords.h"
#include "../helpers/color.h"

const float DEG2RAD = 3.14159/180;

void circle(coords pos, float radius, int red, int green, int blue) {
    glBegin(GL_TRIANGLE_FAN);
    glColor3f(RGB(red), RGB(green), RGB(blue));
    
    for (int i = 0; i < 360; i++) {
        float degInRad = i * DEG2RAD;
        glVertex2f((cos(degInRad) * radius) + pos.x, (sin(degInRad) * radius) + pos.y);
    }
    
    glEnd();
}

void circle(coords pos, float radius, int color) {
    circle(pos, radius, color, color, color);
}