#ifndef CIRCLE_H
#define CIRCLE_H

#include "../helpers/coords.h"

void circle(coords pos, float radius, int red, int green, int blue);
void circle(coords pos, float radius, int color);

#endif