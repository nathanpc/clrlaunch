// square.cpp
// Square stuff

// Include the OpenGL header files.
#ifdef __APPLE__
#include <OpenGL/OpenGL.h>
#include <GLUT/GLUT.h>
#else
#include <GL/glut.h>
#endif

#include "../helpers/color.h"
#include "../helpers/coords.h"

void square(coords pos, int width, int height, int color) {
    //glPushMatrix();
    //glTranslatef(x, y, 0);
    
    glBegin(GL_QUADS);
    glColor3f(RGB(color), RGB(color), RGB(color));
    
    glVertex2d(pos.x + width, pos.y);           // The top right corner.
    glVertex2d(pos.x, pos.y);                   // The top left corner.
    glVertex2d(pos.x, pos.y + height);          // The bottom left corner.
    glVertex2d(pos.x + width, pos.y + height);  // The bottom right corner.
    glEnd();
    
    //glPopMatrix();
}