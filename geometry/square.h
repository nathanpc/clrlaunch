#ifndef SQUARE_H
#define SQUARE_H

#include "../helpers/coords.h"

void square(coords position, int width, int height, int color);

#endif