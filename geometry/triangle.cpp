// triangle.cpp
// Triangle stuff

// Include the OpenGL header files.
#ifdef __APPLE__
#include <OpenGL/OpenGL.h>
#include <GLUT/GLUT.h>
#else
#include <GL/glut.h>
#endif

#include "../helpers/color.h"
#include "../helpers/coords.h"

void triangle(coords pos, int width, int height, int color) {
    glBegin(GL_TRIANGLES);
    glColor3f(RGB((float)color), RGB((float)color), RGB((float)color));
    
    glVertex2f(pos.x, pos.y);                                   // Base left.
    glVertex2f((pos.x + (pos.x + width)) / 2, pos.y - height);  // Top.
    glVertex2f(pos.x + width, pos.y);                           // Base right.
    glEnd();
}