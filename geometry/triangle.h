#ifndef TRIANGLE_H
#define TRIANGLE_H

#include "../helpers/coords.h"

void triangle(coords position, int width, int height, int color);

#endif