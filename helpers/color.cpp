// color.cpp
// Colorizing

#include <math.h>

//float RGB(unsigned int red, unsigned int green, unsigned blue)
float RGB(int color) {
    return (float)color / (float)255;
}