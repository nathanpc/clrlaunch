#ifndef COORDS_H
#define COORDS_H

struct coords {
    double x;
    double y;
};

struct length {
    int width;
    int height;
};

coords double2coords(double x, double y);

#endif