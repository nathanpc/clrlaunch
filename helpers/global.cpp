// global.cpp
// Global settings/stuff

#include <iostream>

int WINDOW_WIDTH = 1024;
int WINDOW_HEIGHT = 600;
const char* TITLE = "clrLaunch";

int ALLIES_COLOR = 210;

int randr(unsigned int min, unsigned int max) {
    int base_random = rand(); /* in [0, RAND_MAX] */
    if (RAND_MAX == base_random) return randr(min, max);
    /* now guaranteed to be in [0, RAND_MAX) */
    int range       = max - min,
    remainder   = RAND_MAX % range,
    bucket      = RAND_MAX / range;
    /* There are range buckets, plus one smaller interval
     within remainder of RAND_MAX */
    if (base_random < RAND_MAX - remainder) {
        return min + base_random/bucket;
    } else {
        return randr(min, max);
    }
}