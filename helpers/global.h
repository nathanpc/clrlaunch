#ifndef GLOBAL_H
#define GLOBAL_H

extern int WINDOW_WIDTH;
extern int WINDOW_HEIGHT;
extern const char* TITLE;

extern int ALLIES_COLOR;

int randr(unsigned int min, unsigned int max);

#endif