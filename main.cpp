// main.cpp
// Where it all starts

#include <iostream>
#include <stdlib.h>

// Include the OpenGL header files.
#ifdef __APPLE__
#include <OpenGL/OpenGL.h>
#include <GLUT/GLUT.h>
#else
#include <GL/glut.h>
#endif

#include "helpers/global.h"
#include "events/event_handlers.h"
#include "scene/scene.h"


using namespace std;

// Initialize 3D rendering.
void initRendering() {
    glEnable(GL_DEPTH_TEST);
    glEnable(GL_COLOR_MATERIAL);
}

int main(int argc, char* argv[]) {
    glutInit(&argc, argv);
    glutInitDisplayMode(GLUT_DOUBLE | GLUT_RGB | GLUT_DEPTH);
    glutInitWindowSize(WINDOW_WIDTH, WINDOW_HEIGHT);
    
    glutCreateWindow(TITLE);
    initRendering();
    
    // Set handler functions for drawing, keypresses and window resizes.
    glutDisplayFunc(drawScene);
    glutKeyboardFunc(handleKeypress);
    glutMouseFunc(handleMouse);
    glutReshapeFunc(handleResize);

    glutTimerFunc(25, update, 0);  // Tell GLUT to call update again in 25 milliseconds.
    glutMainLoop();
    return 0;
}