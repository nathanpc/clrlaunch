//
//  enemy.cpp
//  GeoDefence
//
//  Created by Nathan Campos on 8/1/12.
//  Copyright (c) 2012 Paulino. All rights reserved.
//

#include <iostream>
#include <vector>

#include "../helpers/coords.h"
#include "../helpers/global.h"

using namespace std;

void addEnemy(vector<coords>* enemies, vector<int>* targets, int x, int y) {
    coords pos = { x, y };
    enemies->push_back(pos);
    targets->push_back(randr(0, 3));
}

coords getTargetPos(int target) {
    // Geo.
    coords _geo = { (WINDOW_WIDTH / 2) - 50, WINDOW_HEIGHT };
    int _geo_size = 100;

    // Allies.
    int size = 40;
    coords _ally1 = { ((_geo.x * 3) / 5) - 20, _geo.y - size };
    coords _ally2 = { ((_geo.x * 1) / 5) - 20, _geo.y };
    coords _ally3 = { ((_geo.x * 2) / 5) + (WINDOW_WIDTH / 2) + (_geo_size / 2) - 20, _geo.y - size };
    coords _ally4 = { ((_geo.x * 4) / 5) + (WINDOW_WIDTH / 2) + (_geo_size / 2) - 20, _geo.y - size };
    
    if (target == 0) {
        return _ally1;
    } else if (target == 1) {
        return _ally2;
    } else if (target == 2) {
        return _ally3;
    } else if (target == 3) {
        return _ally4;
    }
    
    return getTargetPos(randr(0, 3));
}