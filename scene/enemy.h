//
//  enemy.h
//  GeoDefence
//
//  Created by Nathan Campos on 8/1/12.
//  Copyright (c) 2012 Paulino. All rights reserved.
//

#ifndef ENEMY_H
#define ENEMY_H

#include <vector>
#include "../helpers/coords.h"

void addEnemy(std::vector<coords>* enemies, std::vector<int>* targets, int x, int y);
coords getTargetPos(int target);

#endif