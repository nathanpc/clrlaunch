// grid.cpp
// Organizes/Draws the application grid

#include <iostream>
#include "../helpers/coords.h"

using namespace std;

int side_padding;

int between_app_space;
length app_rect_size;

length gen_app_rect_size(int icon_max_size, int caption_max_height, int icon_caption_space) {
    length size;
	
    int width = icon_max_size;
    int height = icon_max_size + icon_caption_space + caption_max_height;
	
    size.width = width;
    size.height = height;
	
    return size;
}

void setupGrid(int grid_width, int max_spacing, length app_rect) {
    app_rect_size = app_rect;
    //cout << "W: " << app_rect_size.width << " - H: " << app_rect_size.height << "\n";
    
    // TODO: side_padding = The (rest of the full grid width) / 2
    // Calculate the max amount of squares (no padding)
    int max_pieces = grid_width / app_rect_size.width;
    
    cout << "Max: " << max_pieces << "\n";
    
    if (max_pieces > 0) {
        for (int i = max_pieces - 1; i > 0; i--) {
            cout << i << ": " << grid_width / ((app_rect_size.width * i) + ((max_spacing * i) - 1)) << "\n";
        }
    }
}