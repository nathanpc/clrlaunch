#ifndef GRID_H
#define GRID_H

#include "../helpers/coords.h"

length gen_app_rect_size(int icon_max_size, int caption_max_height, int icon_caption_space);
void setupGrid(int padding, int spacing, length app_rect_size);

#endif