// move.cpp
// Move stuff around

#include <iostream>
#include <cmath>

#include "../helpers/coords.h"

using namespace std;

coords move(coords to, coords obj, int speed, void (*callback)(void)) {
    double direction = atan2(to.y - obj.y, to.x - obj.x) * 180 / M_PI;
    coords result = obj;
    
    if ((floor(obj.x) + speed != to.x || floor(obj.y) + (speed - 1) != to.y) && (floor(obj.x) + (speed - 1) != to.y || floor(obj.y) - speed != to.y)) {
        result.x += cos((direction * M_PI) / 180) * speed;
        result.y += sin((direction * M_PI) / 180) * speed;
        
        if ((floor(result.x) == to.x || floor(result.y) == to.y) && (floor(result.x) == to.x || floor(result.y) == to.y)) {
            callback();
            return result;
        }
        
        return result;
    } else {
        callback();
    }
    
    return result;
}

coords move(coords to, coords obj, int speed) {
    double direction = atan2(to.y - obj.y, to.x - obj.x) * 180 / M_PI;
    coords result = obj;
    
    if ((floor(obj.x) + speed != to.x || floor(obj.y) + (speed - 1) != to.y) && (floor(obj.x) + (speed - 1) != to.y || floor(obj.y) - speed != to.y)) {
        result.x += cos((direction * M_PI) / 180) * speed;
        result.y += sin((direction * M_PI) / 180) * speed;
        
        return result;
    }
    
    return result;
}

coords moveDebug(coords to, coords obj, int speed) {
    double direction = atan2(to.y - obj.y, to.x - obj.x) * 180 / M_PI;
    coords result = obj;
    
    if ((floor(obj.x) + speed != to.x || floor(obj.y) + (speed - 1) != to.y) && (floor(obj.x) + speed != to.y || floor(obj.y) - speed != to.y)) {
        result.x += cos((direction * M_PI) / 180) * speed;
        result.y += sin((direction * M_PI) / 180) * speed;
        
        if ((floor(result.x) == to.x || floor(result.y) == to.y) && (floor(result.x) == to.x || floor(result.y) == to.y)) {
            cout << "STOPPED: " << result.x << " - " << result.y << "\n";
            return result;
        }
        
        cout << to.x << " - " << to.y << "\n";
        cout << result.x << " - " << result.y << "\n";
        cout << floor(result.x) << " - " << floor(result.y) << "\n\n";
        
        return result;
    } else {
        cout << "STOPPED: " << result.x << " - " << result.y << "\n";
    }
    
    return result;
}