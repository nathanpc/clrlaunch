#ifndef MOVE_H
#define MOVE_H

#include "../helpers/coords.h"

coords move(coords to, coords obj, int speed, void (*callback)(void));
coords move(coords to, coords obj, int speed);
coords moveDebug(coords to, coords obj, int speed);

#endif