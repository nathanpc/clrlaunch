// scene.cpp
// Main Screen

#include <iostream>
#include <cmath>
#include <vector>

// Include the OpenGL header files.
#ifdef __APPLE__
#include <OpenGL/OpenGL.h>
#include <GLUT/GLUT.h>
#else
#include <GL/glut.h>
#endif

// Misc.
#include "../helpers/global.h"
#include "../helpers/color.h"
#include "../helpers/coords.h"
#include "move.h"
#include "../events/collision.h"

// Objects.
#include "../geometry/triangle.h"
#include "../geometry/square.h"
#include "../geometry/circle.h"

// Charactes
#include "grid.h"

using namespace std;

bool _first = true;

void setupScene() {
    glClearColor(250, 250, 250, 1);
    //glClearColor(250, 250, 250, 1.0);  //  Set the cleared screen colour to black.
    glViewport(0, 0, WINDOW_WIDTH, WINDOW_HEIGHT);  // This sets up the viewport so that the coordinates (0, 0) are at the top left of the window.
    
    // Set up the orthographic projection so that coordinates (0, 0) are in the top left.
    glMatrixMode(GL_PROJECTION);
    glLoadIdentity();
    glOrtho(0, WINDOW_WIDTH, WINDOW_HEIGHT, 0, -10, 10);
    
    // Back to the modelview so we can draw stuff.
    glMatrixMode(GL_MODELVIEW);
    glLoadIdentity();
    glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT); // Clear the screen and depth buffer.
}

void drawScene() {
    setupScene();
    
    // TODO: DRAW SHIT!
    square((coords){ 100, 100 }, 100, 100, 50);
    
    if (_first) {
        setupGrid(WINDOW_WIDTH, 10, (length){ 50, 50 });
        _first = false;
    }
    
    glutSwapBuffers();  // Send the scene to the screen.
}


void update(int value) {
    // TODO: Update shit!
    
    glutPostRedisplay();  // Tell GLUT that the display has changed.
    glutTimerFunc(25, update, 0);  // Tell GLUT to call update again in 25 milliseconds.
}